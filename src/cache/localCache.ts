import { readFileSync, statSync, writeFileSync } from "fs";
//local
import { Cache } from "../interface/cache";
import { registryEntry } from "interface/registry";
export class LocalCache implements Cache {
  maxSize: number;

  constructor(size: number) {
    this.maxSize = size;
  }
  isAvailable(): boolean {
    return true;
  }

  getData(name: string): Promise<JSON> {
    if (!this.isFileOld(name)) {
      return JSON.parse(readFileSync(`cache/${name}.json`).toString());
    }
  }

  saveData(data: JSON): void {
    const filename: string = this.extractFileName(data);
    this.saveToDir(filename, data);
    this.saveRegistryEntry(filename, data);
  }

  private extractFileName(data: JSON): string {
    const filename: string = JSON.parse(JSON.stringify(data))[0].city_name;
    return filename.replace(/\s/g, "_").toLocaleLowerCase();
  }

  private saveRegistryEntry(filename: string, data: JSON): void {
    const registry: Array<registryEntry> = JSON.parse(
      readFileSync("cache/_registry.json").toString()
    );

    for (const element of registry) {
      if (element["city_name"] === filename.toLocaleLowerCase()) {
        return;
      }
    }
    registry.push(this.generateEntryForRegistry(data));

    this.saveRegistry(registry);
  }

  private generateEntryForRegistry(data: JSON): registryEntry {
    const parsedData = JSON.parse(JSON.stringify(data))[0];
    const payload = {
      cityName: parsedData.city_name.replace(/\s/g, "_").toLocaleLowerCase(),
      lat: parsedData.lat,
      lon: parsedData.lon,
    };
    return payload;
  }

  private saveToDir(filename: string, data: JSON): void {
    writeFileSync(`cache/${filename}.json`, JSON.stringify(data));
  }

  private saveRegistry(registry: registryEntry[]) {
    writeFileSync("cache/_registry.json", JSON.stringify(registry));
  }
  private isFileOld(filename: string): boolean {
    const dayInMilliseconds = 86400000;
    try {
      const file = statSync(`cache/${filename}.json`);
      return Date.now() - Number(file.mtime) > dayInMilliseconds ? true : false;
    } catch (error) {
      return false;
    }
  }
}
