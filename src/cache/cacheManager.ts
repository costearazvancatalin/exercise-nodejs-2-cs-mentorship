import { Cache } from "../interface/cache";

export class CacheManager {
  private workCache: Cache;
  public constructor(cache: Cache) {
    this.workCache = cache;
  }
  public async getData(name: string): Promise<JSON> {
    return await this.workCache.getData(name);
  }
  public saveData(data: JSON): void {
    this.workCache.saveData(data);
  }
}
