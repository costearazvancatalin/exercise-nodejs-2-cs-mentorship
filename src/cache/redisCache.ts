import { readFileSync, writeFileSync } from "fs";
import Redis from "ioredis";
//internal
import { registryEntry } from "../interface/registry";
import { Cache } from "../interface/cache";
export class RedisCache implements Cache {
  private redis: Redis;
  maxSize: number;

  constructor(size: number) {
    this.redis = this.createClient();
    this.maxSize = size;
  }
  isAvailable(): boolean {
    return false;
  }

  async getData(name: string): Promise<JSON> {
    return JSON.parse(await this.redis.get(name));
  }

  saveData(data: JSON): void {
    const secondsInDay = 86400;
    const keyName = this.getKeyName(data);
    this.redis.setex(keyName, secondsInDay, JSON.stringify(data));
    this.saveRegistryEntry(keyName, data);
  }
  private saveRegistryEntry(filename: string, data: JSON) {
    const registry: Array<registryEntry> = JSON.parse(
      readFileSync("cache/_registry.json").toString()
    );

    for (const element of registry) {
      if (element["city_name"] === filename.toLocaleLowerCase()) {
        return;
      }
    }
    registry.push(this.generateEntryForRegistry(data));

    this.saveRegistry(registry);
  }
  private getKeyName(data: JSON): string {
    const keyName: string = JSON.parse(JSON.stringify(data))[0].city_name;
    return keyName.replace(/\s/g, "_").toLocaleLowerCase();
  }

  private saveRegistry(registry: unknown[]) {
    writeFileSync("cache/_registry.json", JSON.stringify(registry));
  }

  private createClient(): Redis {
    const client = new Redis(process.env.REDIS_URI);
    client.on("error", (err: Error) => {
      console.log(err);
    });
    //console.log(client.status);
    return client;
  }
  private generateEntryForRegistry(data: JSON): registryEntry {
    const parsedData: any = JSON.parse(JSON.stringify(data))[0];
    const payload = {
      //i know its a string but still needs to convert it
      cityName: String(parsedData.city_name)
        .replace(/\s/g, "_")
        .toLocaleLowerCase(),
      lat: parsedData.lat,
      lon: parsedData.lon,
    };
    return payload;
  }
}
