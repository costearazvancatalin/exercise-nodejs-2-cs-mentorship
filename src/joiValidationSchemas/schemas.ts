import Joi from "joi";

export const citySchema = Joi.object({
  city: Joi.string().min(3).required(),
});

export const coordinatesSchema = Joi.object({
  lat: Joi.number().min(-90).max(90).required(),
  lon: Joi.number().min(-180).max(180).required(),
});
