// npm packages
import { Router } from "express";
import dotenv from "dotenv";
//schemas
import { citySchema, coordinatesSchema } from "../joiValidationSchemas/schemas";
//middleware
import { validateRequest } from "./middleware/validateRequest";

import { getFromCache } from "./middleware/getFromCache";
import { getFromWeatherApi } from "./middleware/getFromWeatherApi";
import { saveToCache } from "./middleware/saveToCache";

import {
  parseCityName,
  parseCityCoordinates,
} from "./middleware/requestParser";

dotenv.config();

export const routerIndex = Router();

routerIndex.get(
  "/city",
  (req, res, next) => validateRequest(req, res, next, citySchema),
  (req, res, next) => getFromCache(req, res, next, parseCityName),
  getFromWeatherApi
  //saveToCache
);

routerIndex.get(
  "/coordinates",
  (req, res, next) => validateRequest(req, res, next, coordinatesSchema),
  (req, res, next) => getFromCache(req, res, next, parseCityCoordinates),
  getFromWeatherApi
  //saveToCache
);
