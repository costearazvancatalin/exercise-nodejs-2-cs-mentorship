import { Request, Response } from "express";
import swaggerJsdoc from "swagger-jsdoc";

const swaggerDefinition = {
  info: {
    //failOnErrors: true,
    title: "Weather api",
    version: "1.0.0",
    description: "a simple weather api",
  },

  host: "localhost:3000",
  basePath: "/",
  //swagger: "3.0.0",
};

const options = {
  swaggerDefinition,
  apis: ["./docs/*.yaml"],
  //response: ["./docs/response/*.yaml"],
};

const swaggerSpecs = swaggerJsdoc(options);

export function swaggerDocumentation(req: Request, res: Response) {
  res.setHeader("Content-Type", "application/json");
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
  res.setHeader("Referrer-Policy", "no-referrer");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, api_key, Authorization"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  res.json(swaggerSpecs);
}
