import { Request, Response, NextFunction } from "express";
import { ObjectSchema } from "joi";
export async function validateRequest(
  req: Request,
  res: Response,
  next: NextFunction,
  //I can't find the type for the joiValidationSchemas
  validationSchema: ObjectSchema<any>
): Promise<void> {
  try {
    await validationSchema.validateAsync(req.query);
    next();
  } catch (error) {
    res.status(422).send("bad request");
  }
}
