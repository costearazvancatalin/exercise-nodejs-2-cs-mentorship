import { Request } from "express";
import { readFileSync } from "fs";

export function parseCityCoordinates(req: Request) {
  const registry: Array<any> = JSON.parse(
    readFileSync("cache/_registry.json").toString()
  );
  const lat: number = Number(req.query.lat);
  const lon: number = Number(req.query.lon);
  for (const element of registry) {
    if (element.lat == lat && element.lon == lon) {
      return element.city_name;
    }
  }
}
export function parseCityName(req: Request): string {
  return req.query.city.toString().toLocaleLowerCase();
}
