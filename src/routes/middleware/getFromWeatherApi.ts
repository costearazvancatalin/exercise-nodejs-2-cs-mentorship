import { NextFunction, Request, Response } from "express";
import { fetchFromApi } from "./fetchFromApi";

export async function getFromWeatherApi(
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> {
  let response: JSON;
  try {
    response = await fetchFromApi(req);
    res.status(200).json(response);
    res.locals.apiResponse = response;
    next();
  } catch (error) {
    if (process.env.WORKSPACE == "development") {
      console.log(error);
    }
    response = JSON.parse(
      JSON.stringify({ message: "could not find what are you looking for" })
    );
    res.status(404).json(response);
  }
}
