import { LocalCache } from "../../cache/localCache";

import { NextFunction, Request, Response } from "express";
//internal
import { CacheManager } from "../../cache/cacheManager";
const cacheManager = new CacheManager(new LocalCache(100));
export async function getFromCache(
  req: Request,
  res: Response,
  next: NextFunction,
  parseQ: (req: Request) => string
) {
  let response: JSON;
  try {
    response = await cacheManager.getData(parseQ(req));

    if (response) {
      res.status(200).json(response);
      console.log("cache");
      return;
    }
    next();
  } catch (error) {
    if (process.env.WORKSPACE == "development") {
      console.log(error);
    }
    next();
  }
}
