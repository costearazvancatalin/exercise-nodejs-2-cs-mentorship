//npm package
import axios from "axios";
import { Request } from "express";
//internal

export async function fetchFromApi(req: Request): Promise<JSON> {
  const url = `https://api.weatherbit.io/v2.0/current?${generateUrl(req)}&key=${
    process.env.WEATHER_API_KEY
  }`;
  const response: JSON = (await axios.get(url)).data.data;
  return response;
}

function generateUrl(req: Request): string {
  let result = "";
  for (const key in req.query) {
    result += String(key) + "=" + req.query[key] + "&";
  }
  return result.substring(0, result.length - 1);
}
