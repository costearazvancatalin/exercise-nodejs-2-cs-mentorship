export interface registryEntry {
  cityName: string;
  lat: number;
  lon: number;
}
