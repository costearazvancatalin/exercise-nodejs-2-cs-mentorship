export interface Cache {
  getData(name: string): Promise<JSON>;
  saveData(data: JSON): void;
  isAvailable(): boolean;
  maxSize: number;
}
