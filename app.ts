// npm packages
import express from "express";
//internal
import { routerIndex } from "./src/routes/index";
import { swaggerDocumentation } from "./src/routes/swaggerDocs";

const app = express();
const port = 3000;

app.use(express.json());
app.get("/swagger-docs", swaggerDocumentation);

app.use("/index", routerIndex);

app.listen(port, () => {
  console.log("server up and running");
});
