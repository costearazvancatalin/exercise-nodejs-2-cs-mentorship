require("dotenv").config();
import { Random } from "./utilsRandome";
import { timezone, country_code, cardinal_direction } from "./mockArrays";
class Mock {
  constructor() {}

  createResponse(url: string): string {
    const validatorResponse = this.#validateUrl(url);
    if (validatorResponse) {
      return validatorResponse;
    }
    return this.#generate();
  }
  #validateUrl(url: string): string {
    let response;
    if (!this.#validUrl(url)) {
      response = "invalid url";
    }
    if (!this.#validApiKey(url)) {
      response = "invalid key";
    }
    if (!this.#validCoordinates(url)) {
      response = "invalid params lat lon";
    }

    return JSON.stringify(response);
  }
  #generate(): string {
    const random = new Random();
    let response = {
      wind_cdir: random.ValueFromArray(cardinal_direction),
      rh: Math.floor(Math.random() * 100),
      pod: random.ValueFromArray(["d", "n"]),
      lon: random.Float(-180, 180),
      pres: random.Float(995, 1005),
      timezone: random.ValueFromArray(timezone),
      ob_time: random.Date(),
      country_code: random.ValueFromArray(country_code),
      clouds: random.Int(0, 100),
      ts: new Date().getTime(),
      solar_rad: random.Int(0, 10),
      state_code: random.String(2),
      city_name: "City_name-" + random.String(random.Int(5, 10)),
      wind_spd: random.Float(0, 100),
      slp: random.Float(950, 1050),
      wind_cdir_full:
        random.ValueFromArray(cardinal_direction) +
        " imagine it`s not abbreviated",
      sunrise: random.Hour(),
      app_temp: random.Float(-20, 40),
      dni: random.Int(0, 5),
      vis: random.Int(0, 30),
      sources: random.ArrayOfStrings(1, 5, 10, 20),
      h_angle: random.Int(-90, 90),
      dewpt: random.Float(0, 40),
      snow: random.Float(0, 1),
      aqi: random.Int(0, 50),
      dhi: random.Int(0, 10),
      wind_dir: random.Int(0, 360),
      elev_angle: random.Float(-90, 90),
      ghi: random.Int(0, 10),
      precip: random.Float(0, 10),
      sunset: random.Hour(),
      lat: random.Float(-90, 90),
      uv: random.Int(0, 10),
      datetime: random.Date(),
      temp: random.Float(-20, 40),
      weather: {
        icon: random.String(4),
        code: random.Int(0, 1000),
        description: "description and some nonsence " + random.String(50),
      },
      station: random.Char() + random.Int(1, 1000),
    };
    return JSON.stringify(response);
  }

  #validUrl(url) {
    return url.includes("https://api.weatherbit.io/v2.0/current");
  }
  #validCoordinates(url) {
    return url.includes("lat:") && url.includes("lon:");
  }
  #validApiKey(url) {
    return url.includes(`key:${process.env.WEATHER_API_KEY}`);
  }
}

module.exports = Mock;
