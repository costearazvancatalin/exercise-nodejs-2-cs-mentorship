export class Random {
  ValueFromArray(array: string | any[]): string {
    return array[Math.floor(Math.random() * array.length)];
  }

  Float(min: number, max: number): number {
    return Math.random() * (max - min) + min;
  }

  Date() {
    const start = new Date(1975, 0, 0);
    return new Date(
      start.getTime() + Math.random() * (new Date().getTime() - start.getTime())
    );
  }

  Int(min: number, max: number): number {
    return Math.floor(this.Float(min, max));
  }

  Char(): string {
    const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return letters.charAt(Math.floor(Math.random() * letters.length));
  }

  String(size: number): string {
    let result = "";
    for (let i = 0; i < size; i++) {
      result += this.Char();
    }
    return result;
  }

  ArrayOfStrings(
    minSize: number,
    maxSize: number,
    minLength: number,
    maxLength: number
  ): Array<string> {
    const result: string[] = [];
    for (let i = 0; i < this.Int(minSize, maxSize); i++) {
      result.push(this.String(this.Int(minLength, maxLength)));
    }
    return result;
  }

  Hour(): string {
    return "" + this.Int(0, 24) + ":" + this.Int(0, 60);
  }
}
